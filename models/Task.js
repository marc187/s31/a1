const mongoose = require(`mongoose`);

// Create a Schema for tasks
const taskSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, `Name is required`]
        },
        status: {
            type: String,
            default: "pending"
        }
    }
)

// create a model out of the schema
    //mongoose.model(name of the model, Schema where model came from)

    //model is a programming interface that enables us to carry and manipulating database using its methods
module.exports = mongoose.model(`Task`, taskSchema);
    //using module.exports, we allow Task model to be use outside the module