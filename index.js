const express = require(`express`);
const mongoose = require('mongoose');
const dotenv = require(`dotenv`);
const app = express();
const PORT = 3011;

dotenv.config();
app.listen(PORT, console.log(`Listening to port ${PORT}`));

// connect routes to index.js file
const taskRoutes = require(`./routes/taskRoutes`);

//middleware
app.use(express.json())
app.use(express.urlencoded({extended:true}))


mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once(`open`, () => console.log(`Connected to Database`));

app.use(`/api/tasks`, taskRoutes)