const Task = require(`./../models/Task`)

module.exports.createTask = async (reqBody) => {
    return await Task.findOne({name: reqBody.name}).then((result, err) => {
        console.log(result)
        if(result != null && result.name == reqBody.name){
            return `Duplicate Task Found.`
        } else if(reqBody.name.length <= 0) {
            return `Please complete the form.`
        } else {
            let newTask = new Task({
                name: reqBody.name
            })
            newTask.save().then((saveTask, err) => {
                //console.log(saveTask)
                if(saveTask){
                    return true
                } else {
                    return err
                }
            })
        }
    })
}

module.exports.getAllTask = async () => {
    return await Task.find().then((result, err) => {
        if(result){
            return result
        } else {
            return err
        }
    })
}

module.exports.deleteTask = async (taskId) => {
    return await Task.findByIdAndDelete(taskId).then(result => {
        try{
            if(result != null){
                return result
            } else {
                return false
            }
        }catch(err){
            return err
        }
    })
}

module.exports.updateTask = async(taskId, reqBody) => {
    return await Task.findByIdAndUpdate(taskId, { $set: reqBody}, {new: true}).then(result => {
        try{
            if(result){
                return result
            } else {
                return false
            }
        }catch(err){
            return err
        }
    })

}

module.exports.getTask = async (taskId) => {
    return await Task.findById(taskId).then(result => {
        try{
            if(result){
                return result
            } else {
                return false
            }
        }catch(err){
            return err
        }
    })
}

module.exports.completeTask = async (taskId) => {
    return await Task.findByIdAndUpdate(taskId, {$set: {status: `complete`}}, {new: true}).then(result => {
        if(result){
            return result
        } else {
            return false
        }
    })
}


