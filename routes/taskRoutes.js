const { json, response } = require("express");
const express = require(`express`);
const router = express.Router();
const taskController = require(`./../controllers/taskControllers`)

// const {createTask, getAllTask, deleteTask} = require(`./../controllers/taskControllers`)


// ROUTES

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return a message `Duplicate Task found`
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
router.post(`/`, async (req, res) => {
    // console.log(req.body)
    try{
        await taskController.createTask(req.body).then(result => res.send(result))
    } catch(err) {
        res.status(400).json(err.message)
    }
})

// Getting all the tasks

// Business Logic
/*
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

router.get(`/`, async (req, res) => {
    try {
        await taskController.getAllTask().then(result => res.send(result));
    } catch(err){
        res.status(500).json(err)
    }
})

router.delete('/:taskId/delete', async (req, res) => {
    // console.log(req.params)
    try{
        await taskController.deleteTask(req.params.taskId).then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})

router.put('/:taskId', async (req, res) => {
    try{
        await taskController.updateTask(req.params.taskId, req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})


router.get('/:taskId', async (req, res) => {
    try{
        await taskController.getTask(req.params.taskId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

router.put('/:taskId/complete', async (req, res) => {
    try{
        // console.log(req.params.taskId.body)
        await taskController.completeTask(req.params.taskId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

module.exports = router;